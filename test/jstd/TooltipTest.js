TestCase("Tooltip", {
    "test tooltip should raise error without target option":function () {
        assertException("Configuration error", function () {
            new Tooltip();
        }, "Error");
    }
});
