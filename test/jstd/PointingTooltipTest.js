/**
 * @author actuosus
 * Date: 25.12.2011
 * Time: 18:17
 */

TestCase("PointingTooltip", {
    createMouseEvent:function (options) {
        var screenX = options.screenX || 110;
        var screenY = options.screenY || 150;
        var clientX = options.clientX || 110;
        var clientY = options.clientY || 150;
        var event = document.createEvent("MouseEvent");
        event.initMouseEvent("mousemove", true, true, window,
            0, screenX, screenY, clientX, clientY, false, false, false, false, 0, null);
        return event;
    },

    createTarget:function () {
        var target = document.createElement("div");
        target.style.width = "200px";
        target.style.height = "200px";
        target.style.position = "absolute";
        target.style.left = "100px";
        target.style.top = "100px";

        return target;
    },

    "test positioning arrow left":function () {
        var target = this.createTarget();
        var event = this.createMouseEvent({
            screenX:110,
            screenY:150,
            clientX:110,
            clientY:150
        });

        document.body.appendChild(target);

        var pointingTooltip = new PointingTooltip({target:target});
        console.log(event.pageX);
        pointingTooltip.positionArrow(event);
        assertEquals("Arrow should point to the left", "tooltip-arrow tooltip-arrow-left", pointingTooltip.arrow.className);
    },

    "test positioning arrow right":function () {
        var target = this.createTarget();
        var event = this.createMouseEvent({
            screenX:290,
            screenY:150,
            clientX:290,
            clientY:150
        });

        document.body.appendChild(target);

        var pointingTooltip = new PointingTooltip({target:target});

//        console.log(pointingTooltip.targetBoundaries.left);
//        console.log(pointingTooltip.targetBoundaries.right);
//        console.log(pointingTooltip.targetBoundaries.top);
//        console.log(pointingTooltip.targetBoundaries.bottom);
//        console.log(pointingTooltip.containerBoundaries.width);
//        console.log(pointingTooltip.containerBoundaries.height);
//
        console.log(event.pageX, pointingTooltip.targetBoundaries.right - pointingTooltip.container.offsetWidth - 15);
        pointingTooltip.positionArrow(event);
        assertEquals("Arrow should point to the right", "tooltip-arrow tooltip-arrow-right", pointingTooltip.arrow.className);
    },

    "test positioning arrow up":function () {
        var target = this.createTarget();
        var event = this.createMouseEvent({
            screenX:150,
            screenY:110,
            clientX:150,
            clientY:110
        });

        document.body.appendChild(target);

        var pointingTooltip = new PointingTooltip({target:target});

        pointingTooltip.positionArrow(event);
        assertEquals("Arrow should point to the top", "tooltip-arrow tooltip-arrow-up", pointingTooltip.arrow.className);
    },

    "test positioning arrow down":function () {
        var target = this.createTarget();
        var event = this.createMouseEvent({
            screenX:150,
            screenY:290,
            clientX:150,
            clientY:290
        });

        document.body.appendChild(target);

        var pointingTooltip = new PointingTooltip({target:target});

        pointingTooltip.positionArrow(event);
        assertEquals("Arrow should point to the bottom", "tooltip-arrow tooltip-arrow-down", pointingTooltip.arrow.className);
    }
});