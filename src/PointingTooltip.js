/**
 * @author actuosus
 * Date: 25.12.2011
 * Time: 18:16
 * @requires Tooltip
 */

/* globals Tooltip:true */

(function(root, factory) {
    'use strict';

    /* CommonJS */
    if (typeof exports === 'object') {
        module.exports = factory();
    }
    /* AMD module */
    else if (typeof define === 'function' && define.amd) {
        define(factory);
    }
    /* Browser global */
    else {
        root.PointingTooltip = factory();
    }
}(this, function() {
    'use strict';

    /**
     * @class PointingTooltip
     * @augments Tooltip
     * @param {Object} options
     * @param {String=} options.pointerOrigin
     * @param {String=} options.axis
     */
    function PointingTooltip(options) {
        Tooltip.call(this, options);

        this.pointerOrigin = options.pointerOrigin;

        this.axis = options.axis;

        this.arrow = document.createElement("div");
        this.arrow.className = "tooltip-arrow";
        this.container.appendChild(this.arrow);

        this.containerBoundaries = this.getContainerDimensions();
    }

    /**
     * Determines tooltip container dimensions.
     * @returns {{width: Number, height: Number}}
     */
    PointingTooltip.prototype.getContainerDimensions = function () {
        this.container.style.display = "block";
        this.container.style.position = "absolute";
        this.container.style.left = -10000 + "px";
        this.container.style.top = -10000 + "px";
        document.body.appendChild(this.container);
        return {
            width: this.container.offsetWidth,
            height: this.container.offsetHeight
        };
    };

    /**
     * Handles mouse movement event.
     * @param event
     */
    PointingTooltip.prototype.onMouseMove = function tooltipOnMouseOverPrototype(event) {
        if (event.pageX < this.targetBoundaries.left ||
            event.pageX > this.targetBoundaries.right ||
            event.pageY < this.targetBoundaries.top ||
            event.pageY > this.targetBoundaries.bottom) {
            this.hide();
            if (this.hideTimer) {
                clearTimeout(this.hideTimer);
                delete this.hideTimer;
            }
            return;
        }

        var left = event.pageX;
        var top = event.pageY;

        this.positionArrow(event);

        if (this.offset) {
            left += this.offset.x;
            top += this.offset.y;
        }

        if (this.pointerOrigin) {
            switch (this.pointerOrigin) {
                case "center":
                    left = this.targetBoundaries.left + Math.abs(this.targetBoundaries.right - this.targetBoundaries.left) / 2;
                    top = this.targetBoundaries.top + Math.abs(this.targetBoundaries.bottom - this.targetBoundaries.top) / 2;
                    break;
                case "left":
                    left = this.targetBoundaries.left - this.containerBoundaries.width - this.offset.x;
                    top = this.targetBoundaries.top + Math.abs(this.targetBoundaries.bottom - this.targetBoundaries.top) / 2 - (this.containerBoundaries.height / 2);
                    break;
                case "right":
                    left = this.targetBoundaries.right + this.offset.x;
                    top = this.targetBoundaries.top + Math.abs(this.targetBoundaries.bottom - this.targetBoundaries.top) / 2 - (this.containerBoundaries.height / 2);
                    break;
                case "top":
                    left = this.targetBoundaries.left + Math.abs(this.targetBoundaries.right - this.targetBoundaries.left) / 2 - this.containerBoundaries.width / 2 + this.offset.x;
                    top = this.targetBoundaries.top - this.containerBoundaries.height - this.offset.y;
                    break;
                case "bottom":
                    left = this.targetBoundaries.left + Math.abs(this.targetBoundaries.right - this.targetBoundaries.left) / 2 - this.containerBoundaries.width / 2 + this.offset.x;
                    top = this.targetBoundaries.bottom + this.offset.y;
                    break;
            }
        }

        if (this.axis) {
            if (this.axis === 'x') {
                left = event.pageX - this.containerBoundaries.width / 2 + this.offset.x;
            }
            if (this.axis === 'y') {
                top = event.pageY - this.containerBoundaries.height / 2 + this.offset.y;
            }
        }

        this.container.style.left = left + "px";
        this.container.style.top = top + "px";
    };

    /**
     * Positions pointing arrow of the tooltip.
     * @param event Event
     */
    PointingTooltip.prototype.positionArrow = function (event) {
        switch (this.pointerOrigin) {
            case "left":
                this.arrow.className = "tooltip-arrow tooltip-arrow-right";
                return;
            case "right":
                this.arrow.className = "tooltip-arrow tooltip-arrow-left";
                return;
        }
        if (event.pageY < this.targetBoundaries.top + 15) {
            this.arrow.className = "tooltip-arrow tooltip-arrow-up";
        }

        if (event.pageY > this.targetBoundaries.bottom - this.container.offsetHeight - 15) {
            this.arrow.className = "tooltip-arrow tooltip-arrow-down";
        }

        if (event.pageX > this.targetBoundaries.right - this.container.offsetWidth - 15) {
            this.arrow.className = "tooltip-arrow tooltip-arrow-right";
        }

        if (event.pageX < this.targetBoundaries.left + 15) {
            this.arrow.className = "tooltip-arrow tooltip-arrow-left";
        }

        if (this.axis) {
            if (this.axis === 'x') {
                if (this.pointerOrigin === 'top') {
                    this.arrow.className = "tooltip-arrow tooltip-arrow-down";
                }
                if (this.pointerOrigin === 'bottom') {
                    this.arrow.className = "tooltip-arrow tooltip-arrow-up";
                }
            }
            if (this.axis === 'y') {
                if (this.pointerOrigin === 'left') {
                    this.arrow.className = "tooltip-arrow tooltip-arrow-right";
                }
                if (this.pointerOrigin === 'right') {
                    this.arrow.className = "tooltip-arrow tooltip-arrow-left";
                }
            }
        }
    };

    PointingTooltip.prototype.__proto__ = Tooltip.prototype;

    return PointingTooltip;
}));