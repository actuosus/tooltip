/**
 * @author actuosus
 */

(function(root, factory) {
    'use strict';

    /* CommonJS */
    if (typeof exports === 'object') {
        module.exports = factory();
    }
    /* AMD module */
    else if (typeof define === 'function' && define.amd) {
        define(factory);
    }
    /* Browser global */
    else {
        root.Tooltip = factory();
    }
}(this, function() {
    'use strict';

    /**
     @class Tooltip
     @param {Object} options Configuration parameters
     @param {Boolean=} options.fixed
     @param {Element} options.target
     @param {Element=} options.container
     @param {String=} options.title
     @param {HTMLElement|String=} options.content
     @param {Object=} options.offset
     @param {Function=} options.onShow
     @param {Function=} options.onHide
     */
    function Tooltip(options) {
        if (!options) {
            throw new Error("Tooltip configuration required.");
        }
        this.fixed = options.fixed;
        this.container = options.container;
        this.target = options.target;
        this.content = options.content;

        this.visible = false;

        if (!this.target) {
            throw new Error("Tooltip should have target.");
        }

        if (!this.container) {
            this.container = document.createElement("div");
            if (options.className) {
                this.container.className = options.className;
            } else {
                this.container.className = "tooltip";
            }
        }

        if (options.title) {
            this.titleElement = document.createElement("div");
            this.titleElement.className = "tooltip-title";
            this.title = options.title;
            this.titleElement.innerHTML = this.title;
            this.container.appendChild(this.titleElement);
        }

        if (!this.content) {
            this.contentElement = document.createElement("div");
            this.contentElement.className = "tooltip-content";
            this.content = this.target.getAttribute("data-tooltip-text");
            this.contentElement.innerHTML = this.content;
            this.container.appendChild(this.contentElement);
        }

        if (!options.offset) {
            this.offset = {
                x: 0,
                y: 0
            };
        } else {
            this.offset = options.offset;
        }

        this.targetBoundaries = {
            left: this.target.offsetLeft,
            top: this.target.offsetTop,
            right: this.target.offsetLeft + this.target.offsetWidth,
            bottom: this.target.offsetTop + this.target.offsetHeight
        };

        this.timeout = 500;

        // Callbacks
        if (options.onShow) {
            this.onShow = options.onShow.bind(this);
        }
        if (options.onHide) {
            this.onHide = options.onHide.bind(this);
        }

        this.attach();
    }

    Tooltip.prototype.init = function () {};

    Tooltip.prototype.toString = function tooltipToStringPrototype() {
        return "[Tooltip]";
    };

    /**
     * Attaches event listeners for mouse movement.
     */
    Tooltip.prototype.attach = function () {
        this.target.addEventListener("mouseover", this.onMouseOver.bind(this), false);
        if (!this.fixed) {
            this.target.addEventListener("mousemove", this.onMouseMove.bind(this), false);
        }
        this.target.addEventListener("mouseout", this.onMouseOut.bind(this), false);
    };

    Tooltip.prototype.onMouseOver = function tooltipOnMouseEnterPrototype(event) {
        this.onMouseMove(event);
        this.show();

        if (this.hideTimer) {
            clearTimeout(this.hideTimer);
            delete this.hideTimer;
        }
    };

    Tooltip.prototype.onMouseMove = function tooltipOnMouseOverPrototype(event) {
        if (event.pageX < this.targetBoundaries.left ||
            event.pageX > this.targetBoundaries.right ||
            event.pageY < this.targetBoundaries.top ||
            event.pageY > this.targetBoundaries.bottom) {
            this.hide();
            if (this.hideTimer) {
                clearTimeout(this.hideTimer);
                delete this.hideTimer;
            }
            return;
        }

        var left = event.pageX;
        var top = event.pageY;

        if (this.offset) {
            left += this.offset.x;
            top += this.offset.y;
        }

        this.container.style.left = left + "px";
        this.container.style.top = top + "px";
    };

    Tooltip.prototype.onMouseOut = function tooltipOnMouseLeavePrototype(event) {
        this.onMouseMove(event);
        var me = this;

        function hideTimer() {
            me.hide();
            clearTimeout(me.hideTimer);
            delete me.hideTimer;
        }

        if (!this.hideTimer) {
            this.hideTimer = setTimeout(hideTimer, this.timeout);
        }
    };

    Tooltip.prototype.onContainerMouseOver = function tooltipOnContainerMouseOverPrototype(event) {
        this.onMouseMove(event);
        if (this.hideTimer) {
            clearTimeout(this.hideTimer);
            delete this.hideTimer;
        }
    };

    Tooltip.prototype.onContainerMouseMove = function tooltipOnContainerMouseMovePrototype(event) {
        this.onMouseMove(event);
    };

    Tooltip.prototype.onContainerMouseOut = function tooltipOnContainerMouseOutPrototype(event) {
        this.onMouseMove(event);
        var me = this;

        function hideTimer() {
            me.hide();
            clearTimeout(me.hideTimer);
            delete me.hideTimer;
        }

        if (!this.hideTimer) {
            this.hideTimer = setTimeout(hideTimer, this.timeout);
        }
    };

    /**
     * Shows tooltip element.
     * @returns {Tooltip}
     */
    Tooltip.prototype.show = function tooltipShowPrototype() {
        document.body.appendChild(this.container);

        this.container.style.display = "block";
        this.visible = true;

        this.container.addEventListener("mouseover", this.onContainerMouseOver.bind(this), true);
        if (!this.fixed) {
            this.container.addEventListener("mousemove", this.onContainerMouseMove.bind(this), true);
        }
        this.container.addEventListener("mouseout", this.onContainerMouseOut.bind(this), true);

        this.onShow();
        return this;
    };

    /**
     * Hides tooltip element.
     * @returns {Tooltip}
     */
    Tooltip.prototype.hide = function tooltipHidePrototype() {
        this.container.style.display = "none";
        this.visible = false;

        this.onHide();
        return this;
    };

    /**
     * Updates tooltip content.
     * @param {HTMLElement|String} content
     */
    Tooltip.prototype.update = function tooltipUpdatePrototype(content) {
        this.content = content;
        this.contentElement.innerHTML = content;
    };

    Tooltip.prototype.position = function tooltipPositionPrototype() {
    };

    Tooltip.prototype.onShow = function tooltipOnShowPrototype() {
        // console.log("onShow");
    };

    Tooltip.prototype.onHide = function tooltipOnHidePrototype() {
        // console.log("onHide");
    };

    Tooltip.prototype.fadeOut = function tooltipOnHidePrototype() {
    };

    return Tooltip;
}));