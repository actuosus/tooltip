/**
 * @author actuosus@gmail.com <Arthur Chafonov>
 * @file
 * @overview
 * Date: 07/03/14
 * Time: 20:58
 * @copyright
 */

(function(root, factory) {
    'use strict';

    /* CommonJS */
    if (typeof exports === 'object') {
        factory(require('jquery'), require('tooltip'));
    }
    /* AMD module */
    else if (typeof define === 'function' && define.amd) {
        define(['jquery', 'tooltip'], factory);
    }
    /* Browser global */
    else {
        if (!window.Tooltip) {
            throw new Error('Tooltip.js not present');
        }
        factory(window.jQuery, window.Tooltip);
    }
}(function($, Tooltip) {
    'use strict';

    $.fn.tooltip = function(opts) {
        return this.each(function(){
            var $this = $(this),
                data = $this.data();
            if (opts !== false) {
                data.tooltip = new Tooltip(opts);
            }
        });
    };
}));