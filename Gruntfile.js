/**
 * @author actuosus@gmail.com <Arthur Chafonov>
 * @file
 * @overview
 * Date: 07/03/14
 * Time: 20:00
 * @copyright
 */

/* globals module */

module.exports = function (grunt) {
    'use strict';

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            all: {
                options: {
                    jshintrc: true
                },
                src: 'src/*.js'
            }
        },
        karma : {
            options: {
                files: [
                    'test/jstd/*'
                ]
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['src/**/*.js'],
                dest: './build/tooltip.concat.js'
            }
        },
        uglify: {
            options: {
                report: 'min'
            },
            dist: {
                files: {
                    'build/tooltip.packed.js': ['./build/tooltip.concat.js']
                }
            }
        },
        clean: {
            build: ['./build']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-clean');

    grunt.registerTask('default', ['concat', 'uglify']);
};